/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Tetrahedron extends ThreeDimensionalShape{
    private double a,b,c,d,e,f;

    public Tetrahedron() {
        super();
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }
    
    public Tetrahedron(double a, double b, double c, double d, double e, double f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    public double callPri() {
        return a+b+c+d+e+f;
    }

    @Override
    public double callArea() {
        return 0;// cong thuc dai qua nen em bo qua :))
    }

    @Override
    public double Volume() {
        return 0;
    }

    @Override
    public void input() {
        System.out.println("Enter a = ");
        this.a = new Scanner(System.in).nextDouble();
        System.out.println("Enter b = ");
        this.b = new Scanner(System.in).nextDouble();
        System.out.println("Enter c = ");
        this.c = new Scanner(System.in).nextDouble();
        System.out.println("Enter d = ");
        this.d = new Scanner(System.in).nextDouble();
        System.out.println("Enter e = ");
        this.e = new Scanner(System.in).nextDouble();
        System.out.println("Enter f = ");
        this.f = new Scanner(System.in).nextDouble();
    }
    
}
