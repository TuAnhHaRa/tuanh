/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Circle extends TwoDimensionalShape {

    private double r;

    public Circle() {
        super();
    }

    public Circle(double r) {
        this.r = r;
    }
    
    
    public void setR(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    /**
     * Cal primary of circle
     *
     * @return
     */
    @Override
    public double callPri() {
        return 2 * Math.PI * this.r;
    }

    /**
     * Cal Area of circle
     *
     * @return
     */
    @Override
    public double callArea() {
        return this.r * this.r * Math.PI;
    }
    @Override
    public void input(){
        System.out.println("Enter r = ");
        this.r = new Scanner(System.in).nextDouble();
        
    }
}
