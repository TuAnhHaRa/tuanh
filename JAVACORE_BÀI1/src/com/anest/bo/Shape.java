/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

/**
 *
 * @author Nguyen Phong
 */
public interface Shape {

    /**
     *
     * @return 
     */
    public abstract double callArea();

    public abstract double callPri();
        
    public abstract void input();
}
