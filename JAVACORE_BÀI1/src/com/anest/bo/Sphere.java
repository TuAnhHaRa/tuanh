/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Sphere extends ThreeDimensionalShape {

    private double r;

    public Sphere() {
        super();
    }

    public Sphere(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double callPri() {
        return 0;
    }

    @Override
    public double callArea() {
        return 4 * Math.PI * r * r;
    }

    @Override
    public double Volume() {
        return 4 / 3 * Math.PI * r * r * r;
    }

    @Override
    public void input() {
        System.out.println("Enter r = ");
        this.r = new Scanner(System.in).nextDouble();
    }

}
